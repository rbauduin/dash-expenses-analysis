# -*- coding: utf-8 -*-
import dash
import base64
import io
import os
from dash import dcc
from dash import html

# TODO: a CDF type chart for amounts
#       a dat total amount bar chart : daily, weekly, monthly

#       dynamic callbacks? https://community.plot.ly/t/dynamically-creating-callbacks-for-a-dynamically-created-layout/10094
#external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
# loading state css: https://community.plot.ly/t/mega-dash-loading-states/5687/3
external_stylesheets = ['/static/bulma.min.css', 'static/loading_state.css', 'static/local.css']

import pandas as pd
import numpy as np
import csv
import json

from flask_caching import Cache

# initialise app with its cache before defining functions, so they can be memoised
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.css.config.serve_locally = True
app.scripts.config.serve_locally = True
# needed for app.server so be available at cache.init_app (why??? dunno)
# https://community.plot.ly/t/failed-to-find-application-object-server-in-app/13723

server = app.server
# todo: use local css


#app = dash.Dash(__name__)

CACHE_CONFIG = {
    # simple is using SimpleCache, in memory.
    'CACHE_TYPE': 'simple',
}
cache = Cache()
cache.init_app(app.server, config=CACHE_CONFIG)




from datetime import datetime as dt

warning_message = [
    html.H1(className="title is-1", children= "Expenses Analysis"),
dcc.Markdown("""
Welcome to this experimental tool, which is a tool I developed for my personal needs. I share it without any guarantee.


When you first access this tool, a demo CSV file is loaded. Take a look at the other tabs to see the charts generated, as well as
the list of expenses part of the data set in the Table tab.

The form at the top is used to filter expenses. At every change in this form, the charts and the expenses list is updated. The summary
below the form is also updated according to the filtering criteria.

In case you want to contact me, please do so at the address [rblists+expenses@gmail.com](mailto:rblists+expenses@gmail.com) .
"""),

    html.H2(className="title is-2", children="About"),
dcc.Markdown("""
This tool is working with CSV file having the format of CSV export from [HelloExpense](https://play.google.com/store/apps/details?id=com.helloexpense&hl=en). The format is quite generic, and if you use another tool, chances are you can still generate a CSV file handled by this application. See below for a description of the format expected.
"""),
    html.H2(className="title is-2", children="CSV Format"),
dcc.Markdown("""
The CSV has **six columns** and **no header** line. All **values are double quoted**, and the values are **comma separated**.

The columns are, in order:

* date
* category
* amount
* currency
* note
* tags

The currency column is not used, but it needs to be present.

Two types of values formatting are supported: eu-style and us-style. Those apply to columns date and amount, and both columns must be formatted in the same style.

Here are more details about each column:

**date**: in us-style, the format is %m/%d/%y, i.e. numerical month/day/year while is eu-style, it is day/month/year.

**category**: a string value. Not used by the tool currently.

**amount**: a float with 2 decimal digits. In us-style (resp eu-style), the decimal separator is '.'(resp. ','), and the thousands separator is ',' (resp. '.').

**currency**: not used. Values observed in the wild are "€" or "$"

**note**: a string value. This tool aggregates expenses by note too, so reusing note values makes sense.

**tags**: a string denoting a list of tags assigned to the expense. For the tags list consisting of the three elements "my tag", "supermarket" and "cash", the value in the CSV file must be "my tag,supermarket,cash", ie joining the three values with a comma.

Here is an example of the eu-style format:

        "05/01/2018","Other","159,80","€","Garage floor","Car,Home"
        "06/01/2018","Other","210,11","€","Cycling","Sport,Health"
        "06/01/2018","Other","217,96","€","Restaurant","Entertainment,Food"

and here is an example of the us-style format:

        "10/1/2018","Food","5.88","$","With friends","Restaurant"
        "10/1/2018","Others","60.00","$","Costco","Supermarket"


"""),
    html.H2(className="title is-2", children="Data privacy"),

dcc.Markdown("""
When you upload a CSV file, this file is transmitted to the server for analysis and generating the charts. It is only ever kept in memory, and never stored on disk, or logged in any way.
The service is hosted on [Heroku](http://www.heroku.com).
I have no way to look at that data and hence I never look at it. 
If you don't trust me (quite justified to not trust a stranger on the internet), or if your judge your data is too critical to be uploaded, please be logical and do not use this tool.
"""),
    html.Article(className="message is-danger", children=[
        html.Div(className="message-body", children = """
        If accidental leaking of the data you upload to this website is a risk unaffordable to you, do not use this service, and never upload that data to any website either!
        """)
    ])

]
app.layout = html.Div(children=[
                html.H1(children='Hello Expense Analysis', className="title is-1"),
                html.Datalist( id = "amounts_proposed", children = [
                                 html.Option(value=str(v)) for v in np.arange(0,5000,100)
                               ]
                ),
                html.Div(children = [ "When you first access this page, a ", html.A("demo csv file", href="/static/random_expenses.csv"), " is loaded. You can also load your own csv if it is in the correct format." ]) ,

                dcc.Upload(
                        id='upload-data',
                        children=html.Div([
                            'Drag and Drop or ',
                            html.A('Select Files')
                        ]),
                        style={
                            'width': '100%',
                            'height': '60px',
                            'lineHeight': '60px',
                            'borderWidth': '1px',
                            'borderStyle': 'dashed',
                            'borderRadius': '5px',
                            'textAlign': 'center',
                            'margin': '10px'
                        },
                        # Allow multiple files to be uploaded
                        multiple=False
                    ),

                # Filtering form
                html.Div([
                    html.Div([
                            html.Div( id="date_filter_container", children = [
                                html.Label('Date range',className="label"),
                                dcc.DatePickerRange(
                                    id='date-range',
                                    display_format = "DD/MM/YYYY",
                                    first_day_of_week=1,
                                    clearable=True,
                                )
                                ], className = "column"),
                            html.Div( id="notes_filter", children = [
                                html.Label('Selected Notes',className="label"),
                                dcc.Dropdown(
                                    id = "selected-notes",
                                    options=[ ],
                                    multi=True,
                                    )], className="column"),
                            html.Div( id="tags_selection", children=[

                                html.Label('Selected Tags',className="label"),
                                dcc.Dropdown(
                                    id = "selected-tags",
                                    options=[ ],
                                    ),
                                    ], className="column"),
                            html.Div( id="tags_combination", children=[
                                html.Label('Tags combination',className="label"),
                                    dcc.RadioItems(
                                    id = "tags-combination",
                                        options=[
                                            {'label': 'Expenses with Any Selected Tag', 'value': 'any'},
                                            {'label': 'Expenses with All Selected Tag', 'value': 'all'},
                                            ],
                                    value='all',
                                    labelStyle={'display': 'block'}
                                        ),
                                ]),
                        ], className="columns"),
                        html.Div([
                            html.Div( id="minimum_amount_ct", children = [
                                html.Label('Minimum amount (>)',className="label"),
                                dcc.Input(
                                    id = "minimum-amount",
                                    placeholder = "Minimum amount",
                                    type= 'text',
                                    value= '0',
                                )], className="column" ),
                            html.Div( id="maximum_amount_ct", children=[
                                html.Label('Maximum amount (<=)',className="label"),
                                dcc.Input(
                                    id = "maximum-amount",
                                    placeholder = "Maximum amount",
                                    type = 'text',
                                    list = 'amounts_proposed',
                                )], className="column" ),
                            html.Div( id="tags_exclusion", children=[
                                html.Label('Excluded Tags',className="label"),
                                dcc.Dropdown(
                                    id = "excluded-tags",
                                    options=[],
                                    multi=True,
                                )], className="column" ),


                        ], className="columns"),
                        html.Div( id="summary"),
                ], className="container"),

                # Tabs
                dcc.Tabs(id="tabs", value="tab-0", children=[
                    # About tab
                    dcc.Tab(label='About', value='tab-0', children=[
                        html.Div(children = [
                            html.Div(className="", children=[
                                html.Div(className="message-body", children=  warning_message  )
                            ]),

                        ]),
                    ]),
                    # Charts tab
                    dcc.Tab(label='Charts', value='tab-1', children=[
                        html.Div(children = [
                            html.Div([
                                html.Div([
                                    html.Div(id='scatter_of_expenses'),
                                ], className="column"),
                                html.Div([
                                    html.Div(id="bucketed_expenses"),
                                ], className="column")
                            ], className="columns"),
                            html.Div([
                                html.Div([
                                    html.Div(id="chart_sum_per_tag_count"),
                                ], className="column"),
                                html.Div([
                                    html.Div(id="chart_sum_per_note_count"),
                                ], className="column")
                            ], className="columns"),
                        ]),
                    ]),
                    # Timelines tab
                    dcc.Tab(label='Timelines', value='tab-2', children=[
                        html.Div(children = [
                            html.Div([
                                html.Div([
                                    html.Div(id='timeline-barchart'),
                                ], className="column"),
                            ], className="columns"),
                            html.Div([
                                html.Div([
                                    html.Div(id="timeline-weekly"),
                                ], className="column")
                            ], className="columns"),
                            html.Div([
                                html.Div([
                                    html.Div(id="timeline-monthly"),
                                ], className="column")
                            ], className="columns"),
                        ]),
                    ]),
                    # Table tab
                    dcc.Tab(label='Table', value='tab-3', children=[
                        html.Div( id="expenses-table"),
                    ]),

                html.Div(id='signal'),
                html.Div(id='uploaded'),
                #html.Div(id='signal', style={'display': 'none'}),
                ])
            ])

@cache.memoize()
def global_store(e_json,filter_in):
    e = pd.read_json(e_json)
    if "date" in e.columns:
        e = e.sort_values(by="date")
    print("in global sotre, we have {} records".format(str(e.shape)))
    if type(filter_in) is dict:
        filter=filter_in
    else:
        filter=json.loads(filter_in)
    if filter == {}:
        return e
    print("in global store with filter {}".format(filter))
    # initialise df_filter to all True values, so it will be combined with other filters passed
    df_filter = e["date"].map(lambda x : True)
    if filter['start_date']!=None and filter['end_date']!=None:
        df_filter = df_filter & (e.date>=filter['start_date']) & (e.date <= filter["end_date"])
    if filter["selected_tags"] != None and filter["selected_tags"]!=[]:
        selected_tags_set = set(filter["selected_tags"])
        if filter["tags_combination"]=="all":
            df_filter = df_filter & (e.tags.apply(set) >= selected_tags_set)
        elif filter["tags_combination"]=="any":
            df_filter = df_filter & (e.tags.map(selected_tags_set.isdisjoint).map(lambda x: not x))
    if filter["selected_notes"] != None and filter["selected_notes"]!=[]:
        selected_notes_set = set(filter["selected_notes"])
        df_filter = df_filter & (e.note.map(filter["selected_notes"].__contains__))
    if filter["excluded_tags"] != None and filter["excluded_tags"]!=[]:
        excluded_tags_set =  set(filter["excluded_tags"])
        df_filter = df_filter & (e.tags.map(excluded_tags_set.isdisjoint))
    if (filter['minimum_amount'] !='0' ) and (filter['minimum_amount'] !='' ):
        df_filter = df_filter & (e.amount>float(filter['minimum_amount']))
    if (filter['maximum_amount'] !='' ) and (filter['maximum_amount'] != None ):
        df_filter = df_filter & (e.amount<=float(filter['maximum_amount']))
    return e[df_filter]



################################################################################
# callbacks
################################################################################
# To add a new filter:
# - add the filter html element in the layout, giving it a specific id
# - add the dash.dependencies.Input to the function filter_expenses in the last position. First is the id of the html element just added,
#   second is the feature providing the value, usually `value`.
# - add an argument to the function filter_expenses in last position. It will hold the value received from the new filter element
#   specified in the callback
# - add this filter in the `filter` dict un filter_expenses
# - in global_store, handle the new filter criteria, ie build the pandas dataframe filter from this new filter value
# That should be it.


#####################
# datastore functions
#####################

def parse_csv(file_io):
    df = pd.read_csv(file_io, header=None)
    # if float and dates are parsed, we suppose it was in us format
    if df[2].dtype == 'float64' and df[0].dtype=="O":
        print("us options")
        options = {}
        format = "us"
    elif df[2].str.match("-?[\d.]+\,\d{2}").all():
        print("eu options")
        format = "eu"
        options = {
            'parse_dates' : ['date'],
            'date_parser' : (lambda x: pd.datetime.strptime(x, '%d/%m/%Y')),
            'decimal' : ",",
            'thousands' : ".",
        }
    else:
        print("NO FORMAT DETECTED")
    # if we are working on a StringIO object, reinitialise it, as we already read it to detect options
    if 'getvalue' in dir(file_io):
        csv_str = file_io.getvalue()
        file_io = io.StringIO(csv_str)
    e = pd.read_csv(file_io,
                    header=None,
                    **options,
                    #decimal=decimal,
                    #thousands=thousands,
                    #parse_dates=['date'], date_parser=dateparse,
                    quoting = csv.QUOTE_ALL,
                    names=["date","category","amount","currency","note","tags"],
                    dtype={"date":str,"category": str, "amount":float, "currency":str, "note":str, "tags":str}
    )
    e["note"]=e["note"].fillna("")
    e["tags_str"]=e["tags"]
    # split string to get tags list, and remove the empty string tag, generated by the split method....
    e["tags"]=e["tags"].fillna("").str.split(",").map(lambda ts: [t for t in ts if t!=""] )
    return e

@app.callback(dash.dependencies.Output('uploaded', 'children'),
              [dash.dependencies.Input('upload-data', 'contents')],
#              [dash.dependencies.State('upload-data', 'filename'),
#               dash.dependencies.State('upload-data', 'last_modified')]
)
def handle_upload(type_and_content):
    print("in handle_upload")
    #print(type_and_content)
    # return empty dataframe if no file uploaded
    if type_and_content == None:
        return parse_csv('static/random_expenses.csv').to_json()
    print('we have data!')
    content_type, content_string = type_and_content.split(',')
    content_csv = base64.b64decode(content_string)
    e = parse_csv(io.StringIO(content_csv.decode('utf-8')))
    return e.to_json()

def unique_tags():
    np.unique(session["df"].tags.sum())
def unique_notes():
    np.sort(session["df"].note.unique().astype('str'))


#@cache.memoize()
@app.callback(
    dash.dependencies.Output('signal', 'children'),
    [dash.dependencies.Input('date-range', 'start_date'),
     dash.dependencies.Input('date-range', 'end_date'),
     dash.dependencies.Input('selected-tags', 'value'),
     dash.dependencies.Input('tags-combination', 'value'),
     dash.dependencies.Input('excluded-tags', 'value'),
     dash.dependencies.Input('selected-notes', 'value'),
     dash.dependencies.Input('minimum-amount', 'value'),
     dash.dependencies.Input('maximum-amount', 'value'),
     dash.dependencies.Input('uploaded', 'children'),
    ]
)
def filter_expenses(start_date,end_date, selected_tags, tags_combination, excluded_tags, selected_notes, minimum_amount, maximum_amount, df):
    print("in filter_expenses")
    print( 'start date = {}'.format(start_date))
    print( 'end   date = {}'.format(end_date))
    print( 'selected tags = {}'.format(selected_tags))
    print( 'excluded tags = {}'.format(excluded_tags))
    e=pd.read_json(df)
    print( 'e.empty = {}'.format(e.empty))
    # stop here if no data available
    if e.empty:
        return json.dumps({})
    filter = { 'start_date' : start_date,
               'end_date'   : end_date,
               'selected_tags': selected_tags,
               'tags_combination': tags_combination,
               'excluded_tags': excluded_tags,
               'selected_notes': selected_notes,
               'minimum_amount': minimum_amount,
               'maximum_amount': maximum_amount,
    }
    print ("filter = {}".format(filter))
    e=pd.read_json(df)
    res = global_store(df,filter)
    print("we have filtered {} entries".format(res.count()))
    return json.dumps(filter)

####################
# search form fields
####################

def empty_div():
    return html.Div()
def hidden_div(children):
    return html.Div(style= {'display':'none'}, children= children)

# this function returns the field in a hidden div if e is empty
# this is done so that charts are displayed after upload.
# Before this, we didn't create the filtering fields, but then the
# callback filter_expenses is not called because some of its inputs
# are non-existent.
def hide_if_needed(e,field):
    if e.empty:
        return hidden_div(field)
    return field


@app.callback(
    dash.dependencies.Output('date_filter_container', 'children'),
    [dash.dependencies.Input('uploaded', 'children'),
    ],
)
def date_filter(df):
    print("callback for date range called")
    e = pd.read_json(df)
    # if e is empty we set min_date and max_date to None
    # se that we can define field only once, and handle the
    # hiding in hide_if_needed as for other elements.
    # the alternative would be to return a hidden div from here
    # if e is empty, and not call hide_if_needed but return the field
    # directly
    if e.empty:
        min_date = None
        max_date = None
    else:
        min_date=e.date.min()
        max_date=e.date.max()
    field = [
        html.Label('Date range',className="label"),
        dcc.DatePickerRange(
            id='date-range',
            min_date_allowed=min_date,
            max_date_allowed=max_date,
            start_date=min_date,
            end_date=max_date,
            display_format = "DD/MM/YYYY",
            first_day_of_week=1,
        )
    ]
    return hide_if_needed(e,field)


#--------------------------------------------------------------------------------

def unique_notes(e):
    if "note" in e.columns:
        return np.sort(e.note.unique().astype('str'))
    else:
        return []

@app.callback(
    dash.dependencies.Output('notes_filter', 'children'),
    [dash.dependencies.Input('uploaded', 'children'),
    ],
)
def notes_filter(df):
    print("callback for date range called")
    e = pd.read_json(df)
    field = [
            html.Label('Selected Notes',className="label"),
            dcc.Dropdown(
                id = "selected-notes",
                options=[ { 'label' : v , 'value': v} for v in unique_notes(e)],
                multi=True,
            )
    ]
    return hide_if_needed(e,field)

#--------------------------------------------------------------------------------
def unique_tags(e):
    if "tags" in e.columns:
        return np.unique(e.tags.sum())
    else:
        return []


@app.callback(
    dash.dependencies.Output('tags_selection', 'children'),
    [dash.dependencies.Input('uploaded', 'children'),
    ],
)
def tags_selection(df):
    print("callback for tags selection called")
    e = pd.read_json(df)
    field =  [
            html.Label('Selected Tags',className="label"),
            dcc.Dropdown(
                id = "selected-tags",
                options=[ { 'label' : v , 'value': v} for v in unique_tags(e)],
                multi=True,
            )]
    return hide_if_needed(e,field)

#--------------------------------------------------------------------------------
@app.callback(
    dash.dependencies.Output('tags_exclusion', 'children'),
    [dash.dependencies.Input('uploaded', 'children'),
    ],
)
def tags_exclusion(df):
    print("callback for tags exclusion called")
    e = pd.read_json(df)
    field =  [
            html.Label('Excluded Tags',className="label"),
            dcc.Dropdown(
                id = "excluded-tags",
                options=[ { 'label' : v , 'value': v} for v in unique_tags(e)],
                multi=True,
            )]
    return hide_if_needed(e,field)

#--------------------------------------------------------------------------------
@app.callback(
    dash.dependencies.Output('amounts_proposed', 'children'),
    [dash.dependencies.Input('uploaded', 'children'),
    ],
)
def amounts_proposed(df):
    print("callback for amounts_proposed called")
    e = pd.read_json(df)
    if e.empty:
        return []
    return [
            html.Option(value=str(v)) for v in np.arange(0,e.amount.max(),100)
           ]

#--------------------------------------------------------------------------------
@app.callback(
    dash.dependencies.Output('minimum_amount_ct', 'children'),
    [dash.dependencies.Input('uploaded', 'children'),
    ],
)
def minimum_amount(df):
    print("callback for minimum amount called")
    e = pd.read_json(df)
    field = [
            html.Label('Minimum amount (>)',className="label"),
            dcc.Input(
                id = "minimum-amount",
                placeholder = "Minimum amount",
                type= 'text',
                value= '0',
                list = 'amounts_proposed',
            )]
    return hide_if_needed(e,field)
#--------------------------------------------------------------------------------
@app.callback(
    dash.dependencies.Output('maximum_amount_ct', 'children'),
    [dash.dependencies.Input('uploaded', 'children'),
    ],
)
def maximum_amount(df):
    print("callback for maximum amount called")
    e = pd.read_json(df)
    field =  [
            html.Label('Maximum amount (<=)',className="label"),
            dcc.Input(
                id = "maximum-amount",
                placeholder = "Maximum amount",
                type = 'text',
                list = 'amounts_proposed',
            )]
    return hide_if_needed(e,field)



####################
# Charting functions
####################
# every callback has 1 input and 1 state:
# - input = filter, so that when the filter changes, the callback is run
# - state = uploaded dataframe, to be able to pass it to the filter function

# the callback of the filter change builds a filter object that it passes to itw own callback,
# which can then use it to request data in the global store
@app.callback(dash.dependencies.Output('scatter_of_expenses', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def scatter_of_expenses(filter, df):
    print ("in scatter_of_expenses")
    e=global_store(df,filter)
    if e.empty:
        return empty_div()
    layout ={
        'title':'Scatter of expenses vs date',
        'yaxis':{
            'title':'Amount',
            'type':'log',
        },
        "hovermode": "closest"
    }
    return dcc.Graph(
        id='scatter-graph',
        figure={
            'data': [
                {'x': e.date, 'y': e["amount"], 'type': 'scatter', 'name': 'amount', 'mode':'markers','text': e.note + "<br>" + e.tags_str},
            ],
            'layout':layout
        }
    )

#--------------------------------------------------------------------------------

# the callback of the filter change builds a filter object that it passes to itw own callback,
# which can then use it to request data in the global store
@cache.memoize()
@app.callback(dash.dependencies.Output('bucketed_expenses', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def bucketed_expenses(filter,df):
    print ("in bucketed_expenses")
    e = global_store(df,filter)
    if e.empty:
        return empty_div()
    # when I filtered on max amount:
    # e=e[e.amount<max_amount]
    # the +1 is to have intervals of width 10 exactly
    #buckets=np.linspace(0, max_amount, max_amount//10+1)
    buckets=[1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100,200,300,400,500,1000,2000,3000,4000,5000,10000,20000]
    bucketed=e.copy()
    #bucketed['bucket']=pd.cut(e.amount,buckets)
    bucketed['bucket']=pd.cut(e.amount,buckets)
    bucketed['amount']=e.amount
    bucketed.groupby("bucket").size()
    categories = bucketed['bucket'].cat.categories
    ind = np.array([x for x, _ in enumerate(categories)])
    layout ={ 
        'title':'Bucketed expenses',
        'yaxis':{
            'title':'Count'
        },
        'yaxis2':{
            'title':'Amount',
            'titlefont':{
                'color':'rgb(148, 103, 189)'
            },
            'tickfont':{
                'color':'rgb(148, 103, 189)'
            },
            'overlaying':'y',
            'side':'right',
            'rangemode': 'tozero', # to use same X axis as y1
            },

    }
    return dcc.Graph(
        id='bucketed-graph',
        figure={
            'data': [
                {'x': [str(c) for c in categories] , 'y': bucketed.groupby("bucket").size(), 'type': 'bar', 'name': 'count'},
                {'x': [str(c) for c in categories] , 'y': bucketed.groupby("bucket").sum()["amount"], 'type': 'scatter', 'name': 'total', 'yaxis':'y2','mode':'markers'},
                {'x': [str(c) for c in categories] , 'y': bucketed.groupby("bucket").median()["amount"], 'type': 'scatter', 'name': 'median', 'yaxis':'y2','mode':'markers'},
            ],
            'layout' : layout
        }
    )


#--------------------------------------------------------------------------------


def data_for_tags_aggregates(e,filter, group_key):
    if (len(e.index)>0):
        # if by tag, build new dataframe with one tag per row
        if group_key == "tags":
            df = pd.DataFrame({'amount':e.amount.repeat(e.tags.map(len)),'tags':sum(e.tags,[])})
        else:
            df = e
        agg_per_tag = df.groupby(group_key)
        # compute aggregates and sort. We obtain a multiindex (column, aggregate), hence we use a tuple for sorting the values.
        amount_aggregate = agg_per_tag.aggregate(["sum","median","count", "max"]).sort_values(by=("amount","sum"), ascending=False)
        # remove a level of indexing
        agg=amount_aggregate["amount"]

        data = [
                {'x': agg.index, 'y': agg["sum"], 'type': 'bar', 'name': 'total', 'text': 'median: ' + agg["median"].astype('str') + ', count: ' + agg["count"].astype('str') + ', max:' + agg["max"].astype('str')},
                {'x': agg.index, 'y': agg["count"], 'type': 'scatter', 'name': 'count', 'yaxis':'y2', 'mode':'markers'}, #, 'line' : { 'color' : 'rgb(48,255,89)' }
            ]
    else:
        data = []
    return data


def layout_for_tags_and_note_aggregates(agg_type):
    layout ={ 
        'title':'Expenses per {}'.format(agg_type),
        'yaxis':{
            'title':'Total amount'
        },
        'yaxis2':{
            'title':'counts',
            'overlaying':'y',
            'side':'right',
            'anchor':'free',
            'position':0.99,
            'rangemode': 'tozero', # to use same X axis as y1
       }

    }
    return layout

# the callback of the filter change builds a filter object that it passes to itw own callback,
# which can then use it to request data in the global store
@cache.memoize()
@app.callback(dash.dependencies.Output('chart_sum_per_tag_count', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def chart_sum_per_tag(filter,df):
    print ("in chart_sum_per_tag")
    e = global_store(df,filter)
    if e.empty:
        return empty_div()
    data = data_for_tags_aggregates(e,filter, group_key="tags")
    layout = layout_for_tags_and_note_aggregates("tag")

    return dcc.Graph(
        id='tag-aggregates',
        figure={
            'data': data,
            'layout': layout
        }
    )

#--------------------------------------------------------------------------------
# the callback of the filter change builds a filter object that it passes to itw own callback,
# which can then use it to request data in the global store
@cache.memoize()
@app.callback(dash.dependencies.Output('chart_sum_per_note_count', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def chart_sum_per_note(filter,df):
    print ("in chart_sum_per_note")
    e = global_store(df,filter)
    if e.empty:
        return empty_div()
    data = data_for_tags_aggregates(e,filter, group_key="note")
    layout = layout_for_tags_and_note_aggregates("note")
    print(layout)

    return dcc.Graph(
        id='note-aggregates',
        figure={
            'data': data,
            'layout': layout
        }
    )

#--------------------------------------------------------------------------------


@app.callback(dash.dependencies.Output('timeline-barchart', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def timeline_barchart(filter,df):
    print ("in timeline_barchart")
    e = global_store(df,filter)
    if e.empty:
        return empty_div()
    # resampling to have all dates, even those without expenses
    by_date=e.set_index("date").resample('D').sum()
    weekly_rolling = by_date.rolling(7).mean()
    layout ={ 
        'title':'Timeline total expenses',
        'yaxis':{
            'title':'Total'
        },

    }
    return dcc.Graph(
        id='timeline-barchart-chart',
        figure={
            'data': [
                {'x': by_date.index , 'y': by_date["amount"], 'type': 'bar', 'name': 'sum'},
                {'x': weekly_rolling.index , 'y': weekly_rolling["amount"], 'type': 'scatter', 'name': 'weekly rolling avg'},
            ],
            'layout' : layout
        }
    )


#--------------------------------------------------------------------------------

@app.callback(dash.dependencies.Output('timeline-weekly', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def timeline_weekly(filter,df):
    print ("in timeline_weekly")
    e = global_store(df,filter)
    if e.empty:
        return empty_div()
    # resampling to have all dates, even those without expenses
    by_date=e.set_index("date").resample('W-MON').sum()
    weekly_rolling = by_date.rolling(4).mean()
    layout ={ 
        'title':'Timeline daily expenses',
        'yaxis':{
            'title':'Total'
        },

    }
    return dcc.Graph(
        id='timeline-weekly-chart',
        figure={
            'data': [
                {'x': by_date.index , 'y': by_date["amount"], 'type': 'bar', 'name': 'sum'},
                {'x': weekly_rolling.index , 'y': weekly_rolling["amount"], 'type': 'scatter', 'name': '4 weeks rolling avg'},
            ],
            'layout' : layout
        }
    )


#--------------------------------------------------------------------------------

@app.callback(dash.dependencies.Output('timeline-monthly', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def timeline_monthly(filter,df):
    print ("in timeline_monthly")
    e = global_store(df,filter)
    if e.empty:
        return empty_div()
    # resampling to have all dates, even those without expenses
    by_month=e.set_index("date").resample('MS').sum()
    by_day=e.set_index("date").resample('D').sum()
    seven_day_avg = by_day.rolling(7).mean()
    thirty_day_sum = by_day.rolling(30).sum()
    layout ={ 
        'title':'Timeline monthly expenses',
        'yaxis':{
            'title':'Total'
        },
        'bargap': 0,
        'boxgap': 0,

    }
    return dcc.Graph(
        id='timeline-monthly-chart',
        figure={
            'data': [
                {'x': by_month.index , 'y': by_month["amount"], 'type': 'bar', 'name': 'Monthly expense', 'offset': -0.5},
                {'x': by_day.index , 'y': by_day["amount"], 'type': 'scatter', 'name': 'daily expense'},
                {'x': thirty_day_sum.index , 'y': thirty_day_sum["amount"], 'type': 'scatter', 'name': '30 days rolling sum'},
            ],
            'layout' : layout
        }
    )




@app.callback(dash.dependencies.Output('expenses-table', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def generate_table(filter,df):
    e = global_store(df,filter)
    if e.empty:
        return empty_div()
    if "tags" in e.columns:
        e = e.drop("tags", axis="columns")
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in e.columns])] +

        # Body
        [html.Tr([
            html.Td(e.iloc[i][col]) for col in e.columns
        ]) for i in range(len(e))]
        ,className="table is-striped is-fullwidth"
    )

#--------------------------------------------------------------------------------
@app.callback(dash.dependencies.Output('summary', 'children'),
              dash.dependencies.Input('signal', 'children'),
              dash.dependencies.State('uploaded', 'children')
)
def generate_summary(filter,df):
    e = global_store(df,filter)
    if e.empty:
        return empty_div()
    if "tags" in e.columns:
        e=e.drop("tags", axis="columns")
    amount_description = e["amount"].describe()
    # do a list comprehension over the list of fields to be displayed, each having a label and the value in one column
    return  html.Div( 
                    [ html.Div( [ html.Label(field, className="label"), amount_description.loc[field].round(2)], className="column" )
                                 for field in ["count","mean","std", "min","25%","50%","75%","max"]
                    ] + [
                       html.Div( [ html.Label("total", className="label"), e.amount.sum()], className="column" )
                    ]
            , className="columns"),

# For serving local static files
@app.server.route('/static/<path:path>')
def static_file(path):
    static_folder = os.path.join(os.getcwd(), 'static')
    print("static filter:")
    print(static_folder)
    print(path)
    return send_from_directory(static_folder, path)

if __name__ == '__main__':
    app.run_server(debug=True,host= "0.0.0.0")
